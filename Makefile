install:
	python -m pip install -r requirements.txt
	cp cloudflare-ddns /usr/local/bin/cloudflare-ddns
	chmod +x /usr/local/bin/cloudflare-ddns
	mv cf-update.service /usr/lib/systemd/system/cf-update.service
	mv cf-update.timer /usr/lib/systemd/system/cf-update.timer
